sqlplus system/admin;

create tablesapce nnblog
datafile '.../nnblog.dbf'
size 100m;

create user nnblog identified by nnblog default tablespace nnblog;
grant connect to nnblog;
grant resource to nnblog;

connect nnblog/nnblog;


create table hobby(
       id number(10) primary key,
       name varchar(50),
       code number(2) unique
);
create sequence hobby_id start with 0 increment by 1 minvalue 0;
insert into hobby values(hobby_id.nextval,'学习',1);
insert into hobby values(hobby_id.nextval,'唱歌',2);
insert into hobby values(hobby_id.nextval,'看书',3);
insert into hobby values(hobby_id.nextval,'爬山',4);
insert into hobby values(hobby_id.nextval,'写代码',5);
insert into hobby values(hobby_id.nextval,'调bug',5);
commit;
select * from hobby;

create table nativePlace(
       id number(10) primary key,
       name varchar(50),
       code varchar(6) 
);
alter table NATIVEPLACE add constraint unique1 unique (CODE);


create sequence nativePlace_id start with 0 increment by 1 minvalue 0;
insert into nativePlace values(nativePlace_id.nextval,'山东','37');
insert into nativePlace values(nativePlace_id.nextval,'济南','3701');
insert into nativePlace values(nativePlace_id.nextval,'̩泰安','3709');
insert into nativePlace values(nativePlace_id.nextval,'北京','01');
insert into nativePlace values(nativePlace_id.nextval,'朝阳区','0101');
commit;
select * from nativePlace;

select * from nativePlace where length(code)=2;
select * from nativePlace where length(code)=4 and substr(code,1,2)='37';


create table users(
       id number(10) primary key,
       name varchar2(50) not null,
       password varchar2(50) not null,
       sex char(1) default '1',
       email varchar2(100)
);

select * from users;

create sequence users_id start with 0 increment by 1 minvalue 0 maxvalue 99999999;

insert into users values(users_id.nextval,'admin','admin','1','albert@qq.com');
insert into users values(users_id.nextval,'test','test','0','john@qq.com');
commit;

create table userDetail(
       id number(10) primary key,
       nativeplace_code varchar2(6) references nativePlace(code),
       hobby_code varchar2(50)
);
alter table USERDETAIL add constraint foreign1 foreign key (NATIVEPLACE_CODE) references nativeplace (CODE);
create sequence userdetail_id start with 0 increment by 1 minvalue 0;

select table_name from user_tables;

select u.id,u.name,u.password,u.sex,u.email,np.name,ud.hobby_code from users u left outer join Userdetail ud on u.id=ud.userid
left outer join nativeplace np on ud.nativeplace_code=np.code;