<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>用户注册</title>
		<link rel="stylesheet" type="text/css" href="css/base.css"/>
		<script src="js/ajaxUtil.js" type="text/javascript" charset="utf-8"></script>
		<script src="js/register.js" type="text/javascript" charset="utf-8"></script>
	</head>
	<body>
		<article>
			<header>
				<h1>用户注册</h1>
			</header>
			<section>
				<fieldset id="uerRegister">
					<legend hidden>用户注册</legend>
					<table class="registerTable">
						<tbody id="registerTbody">
							<tr>
								<td><label for="userName">用户名：</label></td>
								<td><input type="text" name="userName" id="userName" value="" /></td>
								<td>
									<div id="userNameImage"></div>
									<div id="userNameMessage"></div>
								</td>
							</tr>
							<tr>
								<td><label for="password">密码：</label></td>
								<td><input type="password" name="password" id="password" value="" /></td>
								<td>
									<div id="passwordImage"></div>
									<div id="passwordMessage"></div>
								</td>
							</tr>
							<tr>
								<td><label for="rePassword">重复密码：</label></td>
								<td><input type="password" name="rePassword" id="rePassword" value="" /></td>
								<td>
									<div id="rePasswordImage"></div>
									<div id="rePasswordMessage"></div>
								</td>
							</tr>
							<tr>
								<td>性别：</td>
								<td colspan="2">
									<input type="radio" name="sex" id="man" value="1" />
									<label for="man">男</label>&nbsp;&nbsp;&nbsp;&nbsp;
									<input type="radio" name="sex" id="woman" value="1" />
									<label for="woman">女</label>
								</td>
							</tr>
							<tr>
								<td>爱好：</td>
								<td colspan="2">
								<table>
									<tr>
										<c:forEach items="${requestScope.hobbies}" var="hobby" varStatus="status">
											<td>
												<input type="checkbox" name="fav" id="${hobby.code}" value="${hobby.code}" />
												<label for="${hobby.code}">${hobby.name}</label>
											<td>
											<c:if test="${(status.index+1)%4==0}"></tr><tr></c:if>
											
										</c:forEach>
									</tr>
								</table>
								</td>
							</tr>
							<tr>
								<td>来自于：</td>
								<td colspan="2">
									<select id="selProvince">
									<c:forEach items="${requestScope.provinces}" var="province">
										<option value="${province.code}">${province.name}</option>
									</c:forEach>
									</select>&nbsp;&nbsp;&nbsp;&nbsp;
									<select id="selCity"></select>
								</td>
							</tr>
							<tr>
								<td>email</td>
								<td><input type="text" name="email" id="email"></td>
								<td>
									<div id="emailImage"></div>
									<div id="emailMessage"></div>
								</td>
							</tr>
						</tbody>
					</table>
					<nav id="btnOperation">
						<button type="reset" class="btn">重置</button>&nbsp;&nbsp;
						<button type="button" class="btn">注册</button>
					</nav>
				</fieldset>
			</section>
		</article>
	</body>
</html>
