package dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import db.utils.JdbcTemplate;

import dao.iface.NativePlaceDao;
import domain.NativePlace;

public class NativePlaceJdbcDaoImpl implements NativePlaceDao{

	@Override
	public List<NativePlace> getLevel1() {
		String sql="select * from nativePlace where length(code)=2";
		List<NativePlace> list=JdbcTemplate.query(sql, (rs)->{
			List<NativePlace> list0=new ArrayList<>();
			try {
				while(rs.next()) {
					NativePlace np=new NativePlace(rs.getInt(1),rs.getString(2),rs.getString(3));
					list0.add(np);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
			return list0;
		}, null);
		list.add(0,new NativePlace(0,"请选择省份","00"));
		return list;
	}

	@Override
	public List<NativePlace> getLevel2(String code) {
		String sql="select * from nativePlace where length(code)=4 and substr(code,1,2)=?";
		List<NativePlace> list=JdbcTemplate.query(sql, (rs)->{
			List<NativePlace> list0=new ArrayList<>();
			try {
				while(rs.next()) {
					NativePlace np=new NativePlace(rs.getInt(1),rs.getString(2),rs.getString(3));
					list0.add(np);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
			return list0;
		}, new Object[] {code});
		return list;
	}

}
