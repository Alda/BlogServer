package dao;

import db.utils.JdbcTemplate;

import dao.iface.UserDao;

public class UserJdbcDaoImpl implements UserDao {

	@Override
	public int checkUser(String name, String password) {
		int rowNum=0;
		String sql="select count(1) from users where name=? and password=?";
		rowNum=JdbcTemplate.queryForCount(sql, name,password);
		return rowNum;
	}
	
}
