package dao.iface;

import java.util.List;

import domain.Hobby;

public interface HobbyDao {
	public List<Hobby> getHobbies();
}
