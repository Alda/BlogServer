package dao.iface;

import java.util.List;

import domain.NativePlace;

public interface NativePlaceDao {

	public List<NativePlace> getLevel1();
	public List<NativePlace> getLevel2(String code);
}
