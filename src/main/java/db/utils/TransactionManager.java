package db.utils;

import java.sql.SQLException;

import org.apache.log4j.Logger;

public class TransactionManager {

	private static Logger log=Logger.getLogger(TransactionManager.class);
	
	public void beginTranscation() {
		try {
			DBConnection.getConnection().setAutoCommit(false);
		} catch (SQLException e) {
			log.error("transaction begin error,the message is:"+e.getMessage());
		}
	}
	
	public void commitAndClose() {
		try {
			DBConnection.getConnection().commit();
		} catch (SQLException e) {
			log.error("transaction commit error,the message is:"+e.getMessage());
			try {
				DBConnection.getConnection().rollback();
			} catch (SQLException e1) {
				log.error("transaction rollback error,the message is:"+e1.getMessage());
			}
		} finally {
			try {
				DBConnection.getConnection().setAutoCommit(true);
				DBConnection.getConnection().close();
			} catch (SQLException e) {
				log.error("close connection error,the message is:"+e.getMessage());
			}
		}
	}
	
	public void rollbackandClose() {
		try {
			DBConnection.getConnection().rollback();
		} catch (SQLException e) {
			log.error("transaction rollback error,the message is:"+e.getMessage());
		} finally {
			try {
				DBConnection.getConnection().setAutoCommit(true);
				DBConnection.getConnection().close();
			} catch (SQLException e) {
				log.error("close connection error,the message is:"+e.getMessage());
			}
		}		
	}
}
