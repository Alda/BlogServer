package service.iface;

import java.util.List;

import domain.NativePlace;

public interface NativePlaceService {

	public List<NativePlace> getProvince();
	public List<NativePlace> getCity(String code);
}
