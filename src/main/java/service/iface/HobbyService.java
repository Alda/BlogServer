package service.iface;

import java.util.List;

import domain.Hobby;

public interface HobbyService {
	public List<Hobby> getHobbies();
}
