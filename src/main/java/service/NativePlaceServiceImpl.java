package service;

import java.util.List;

import dao.NativePlaceJdbcDaoImpl;
import dao.iface.NativePlaceDao;
import domain.NativePlace;
import service.iface.NativePlaceService;

public class NativePlaceServiceImpl implements NativePlaceService{

	private NativePlaceDao nativePlaceDao=new NativePlaceJdbcDaoImpl();
	
	@Override
	public List<NativePlace> getProvince() {
		return nativePlaceDao.getLevel1();
	}

	@Override
	public List<NativePlace> getCity(String code) {
		return nativePlaceDao.getLevel2(code);
	}

}
