package service;

import java.util.List;

import dao.HobbyJdbcDaoImpl;
import dao.iface.HobbyDao;
import domain.Hobby;
import service.iface.HobbyService;

public class HobbyServiceImpl implements HobbyService{

	private HobbyDao hobbyDao=new HobbyJdbcDaoImpl();
	
	public List<Hobby> getHobbies(){
		return hobbyDao.getHobbies();
	}
}
