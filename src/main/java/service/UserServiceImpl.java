package service;

import dao.UserJdbcDaoImpl;
import dao.iface.UserDao;
import service.iface.UserService;

public class UserServiceImpl implements UserService{

	private UserDao userDao=new UserJdbcDaoImpl();
	
	@Override
	public boolean login(String name, String password) {
		boolean flag=false;
		if(userDao.checkUser(name, password)==1) {
			flag=true;
		}
		return flag;
	}
	
	public void main(String args[]) {
		int a = userDao.checkUser("admin","admin");
		System.out.println(a);
	}
}
