package web.servlet;

import java.io.IOException;
import java.util.List;
import java.util.Objects;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.HobbyJdbcDaoImpl;
import dao.iface.HobbyDao;
import domain.Hobby;
import domain.NativePlace;
import service.NativePlaceServiceImpl;
import service.UserServiceImpl;
import service.iface.NativePlaceService;
import service.iface.UserService;


@WebServlet("/user.servlet")
public class UserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private UserService userService=new UserServiceImpl();
	private HobbyDao hobbyDao=new HobbyJdbcDaoImpl();
	private NativePlaceService nativePlaceService=new NativePlaceServiceImpl();

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		response.setContentType("text/html; charset=utf-8");
		
		String param=request.getParameter("param");
		
		String name=request.getParameter("userName");
		String password=request.getParameter("password");
		
		String isCookie=null==request.getParameter("isCookie")?"no":request.getParameter("isCookie");
		
		if(Objects.equals("register", param)) {
			List<Hobby> hobbyList=hobbyDao.getHobbies();
			List<NativePlace> provinceList=nativePlaceService.getProvince();
			request.setAttribute("hobbies", hobbyList);
			request.setAttribute("provinces", provinceList);
			request.getRequestDispatcher("register.jsp").forward(request, response);
		}
		if(Objects.equals("login",param)) {
			if(userService.login(name, password)) {
				//cookie操作
				if(Objects.equals(isCookie, "yes")) {
					Cookie cookie=new Cookie("web","www.nnblog.com");
					cookie.setMaxAge(60*60*24);
					response.addCookie(cookie);
				}
				HttpSession session=request.getSession();
				session.setAttribute("name", name);
				request.getRequestDispatcher("views/main.jsp").forward(request, response);
			}else {
				request.setAttribute("msg","用户名或密码错误!!!");
				request.getRequestDispatcher("login.jsp").forward(request, response);
			}
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
