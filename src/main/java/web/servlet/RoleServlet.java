package web.servlet;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class RoleServlet
 */
@WebServlet(
		urlPatterns = { "/role.servlet" }, 
		initParams = { @WebInitParam(name = "char", value = "utf-8")})
public class RoleServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	public String charset;
	
	public void init(ServletConfig config) {
		charset=config.getInitParameter("char");
	}
       

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding(charset);
		response.getWriter().append("Served at: "+charset);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
