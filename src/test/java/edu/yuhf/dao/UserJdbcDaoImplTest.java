package edu.yuhf.dao;

import org.junit.Test;

import dao.UserJdbcDaoImpl;
import dao.iface.UserDao;

public class UserJdbcDaoImplTest {

	private UserDao userDao=new UserJdbcDaoImpl();
	
	@Test
	public void checkUserTest() {
		int rowNum=userDao.checkUser("admin", "admin");
		System.out.println("checkUser() return:"+rowNum);
	}
}
