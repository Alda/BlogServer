package edu.yuhf.dao;

import java.util.List;

import org.junit.Test;

import dao.HobbyJdbcDaoImpl;
import dao.iface.HobbyDao;
import domain.Hobby;

public class HobbyJdbcDaoImplTest {

	private HobbyDao hobbyDao=new HobbyJdbcDaoImpl();
	
	@Test
	public void getHobbyTest() {
		List<Hobby> list=hobbyDao.getHobbies();
		list.stream().forEach((item)->System.out.println(item.getName()));
	}
}
